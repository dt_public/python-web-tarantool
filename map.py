import asyncio
import json
import uuid

import asynctnt
from aiohttp import web

conn = None

# Отдаём фронтенд
async def index(request):
    return web.FileResponse('./index.html', headers={"Content-type": "text/html; charset=utf-8"})

# Обработчик для создания новой геоточки
async def new(request):
    data = await request.json()
    # Генерируем уникальный идентификатор
    data['id'] = str(uuid.uuid4())
    # Вставляем в Tarantool
    await conn.insert("geo", [data['id'], data['coordinates'], data['comment']])
    return web.json_response([data])

# Удаление геоточки
async def remove(request):
    data = await request.json()
    await conn.delete("geo", [data['id']])
    return web.json_response([data])

# Возврат всех геоточек в запрашиваемом регионе
async def lst(request):
    rect = request.rel_url.query['rect']
    rect = json.loads(rect)
    # Запрашиваем 1000 точек из базы
    res = await conn.select('geo', rect, index="geoidx", iterator="LE", limit=1000)
    result = []
    for tuple in res.body:
        result.append({
            "id": tuple['id'],
            "coordinates": tuple['coordinates'],
            "comment": tuple['comment'],
        })
    return web.json_response(result)

async def main():
    global conn
    # Подключаемся в базе
    conn = asynctnt.Connection(host='127.0.0.1', port=3301, username="storage", password="passw0rd")
    await conn.connect()

    # Создаём таблицу для геоточек
    await conn.call("box.schema.space.create", ["geo", {"if_not_exists": True}])
    await conn.call("box.space.geo:format", [[
        {"name": "id", "type": "string"},
        {"name": "coordinates", "type": "array"},
        {"name": "comment", "type": "string"},
    ]])

    # Создаём первичный ключ на таблицу
    await conn.call("box.space.geo:create_index", ["primary", {"parts":["id"], "if_not_exists": True}])
    # Создаём геоиндекс
    await conn.call("box.space.geo:create_index", ["geoidx", {"parts":["coordinates"], "type":"RTREE", "unique":False, 
        "if_not_exists": True}])

    app = web.Application()
    app.add_routes([web.get('/', index),
                web.get('/list', lst),
                web.post('/new', new),
                web.post('/remove', remove)])

    await asyncio.gather(web._run_app(app))

asyncio.run(main())