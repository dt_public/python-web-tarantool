FROM python:3.9

ADD main.py .
ADD index.html .

RUN pip install asynctnt aiohttp
CMD [“python”, “./main.py”] 